const express = require('express');
const router = express.Router();
const {errorHandler} = require('../bin/custom-errors-handler');
const content = require('../_fake-data/content');

router.get('/:contentType', async (req, res) => {
  const {contentType} = req.params;

  try {
    if (contentType === 'wedding') {
      console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
      console.log('type is wedding');
      console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
    }
    return res.status(200).send(content);
  } catch (e) {
    return errorHandler(500, `get content ${contentType} failed`, res, `${req.originalUrl} get content ${contentType} failed: ${e}`);
  }
});

module.exports = router;
