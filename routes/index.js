const express = require('express');
const router = express.Router();
const contentRoute = require('./content');
const authRoute = require('./auth');
router.use('/auth', authRoute);
router.use('/content', contentRoute);

module.exports = router;
