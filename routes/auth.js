const express = require('express');
const router = express.Router();
const {errorHandler} = require('../bin/custom-errors-handler');
const authData = require('../_fake-data/auth');
const cryptor = require('../bin/encrypt');
// const { generateToken } = require('../bin/tokens-verifier');



router.post('/login', async (req, res) => {
  'use strict';
  // eslint-disable-next-line
  const { email, password } = req.body;
  const userData = cryptor.encrypt(JSON.stringify({ email, password }));
  // let userObj;
  try {
    // userObj = await AuthSchema.findOneAndUpdate({ userData }, { signedIn: Date.now() });

    // if (!userObj) {
    //   throw new Error('user object is empty');
    // }
  } catch (e) {
    return errorHandler(
     401,
     'Incorrect User email or/and Password',
     res,
     `${req.originalUrl} AuthSchema findOne: ${e}`,
    );
  }
  // generateToken({ email, password })
  const token = 'test_token';
  return res.status(200).send({ token });
});

module.exports = router;
