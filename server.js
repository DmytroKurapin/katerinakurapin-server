require('dotenv').config({ path: './variables.env' });
const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const port = process.env.PORT || '3000';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Allow CORS support and remote requests to the service
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE');
  res.setHeader(
   'Access-Control-Allow-Headers',
   'x-requested-with, Origin, Authorization, Accept, Content-Type, X-Auth-Token',
  );
  next();
});

const routes = require('./routes');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/_fake-data'));
app.use(express.static(path.join(__dirname, '../katerinakurapin/src')));

app.use('/api', routes);
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../katerinakurapin/src/index.html'));
});

const server = http.createServer(app);
server.listen(port);
server.on('listening', () => {
  console.log('Express app start on port ' + port); // todo logger
});
