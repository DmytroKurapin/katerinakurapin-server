const jwt = require('jsonwebtoken');
const tokenExpiration = '8h';
const jwtPrivateKey = new Buffer(process.env.TOKEN_PRIVATE_KEY, 'base64'); // private key for encoding/decoding data

exports.verifyAuthToken = function(receivedToken) {
  'use strict';
  try {
    jwt.verify(receivedToken, jwtPrivateKey);
  } catch (err) {
    console.log('Error happened on verifying token', err); // TODO logger catch error
    return false;
  }
  return true;
};

exports.generateToken = function(dataToEncode) {
  return jwt.sign(dataToEncode, jwtPrivateKey, { /* algorithm: 'RS256', */ expiresIn: tokenExpiration });
};
