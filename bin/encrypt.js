let crypto;
try {
  crypto = require('crypto');
} catch (err) {
  console.log('crypto support is disabled!'); //todo add logs
}

exports.encrypt = text => {
  let cipher = crypto.createCipher('aes-256-cbc', process.env.CRYPTO_KEY);
  let crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
};

exports.decrypt = text => {
  let decipher = crypto.createDecipher('aes-256-cbc', process.env.CRYPTO_KEY);
  let dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
};
